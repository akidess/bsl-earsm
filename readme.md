The BSL-EARSM turbulence model of Menter etal (2012) has been implemented in
OpenFOAM 1.7.x.  In order to use this model, clone the folder and do a `wmake libso`.
The model can be referenced as 'BSLEARSM_LowRe94w1cp' to simulate a
flow, as used in the square duct example case.  To use this custom
dynamic library, it has to be referenced in the controlDict file as,
`libs ( "libEARSMincompressibleRASModels.so" );`

A verification of the implementation was performed, with reference to the square
duct predictions of Menter et al (2012) and documented in `squareDuctVerification.pdf`.
The model is similar to the implementation in CFX, except for few differences
noted below.  The model solves for a Poisson equation in the first iteration to evaluate the wall
distance.  A low-Reynolds number boundary condition is used for omega.  The
source code will need changes to be used with newer OpenFOAM versions.

### CFX model
The equivalent model in ANSYS CFX-14 is the 'BSL EARSM' model.  However, there are two
differences.
- $P_1$ and $P_2$ have the coefficient of $II_s$ to be different from the below
  reference.  For $P_1$ the coeff. is 0.466875, while reference uses 0.45.  For
  $P_2$ the coeff. is 0.93375, while the reference uses 0.9.  This has a minor
  influence.
- The turbulence timescale does not use the Durbin limiter (reference).  This influences the near-wall predictions.

### Reference
Menter, F. R., A. V. Garbaruk, and Y. Egorov. "Explicit algebraic Reynolds
stress models for anisotropic wall-bounded flows." In *Progress in Flight
Physics*, vol. 3, pp. 89-104. EDP Sciences, 2012.
